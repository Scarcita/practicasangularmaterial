import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-material-escolar',
  templateUrl: './material-escolar.component.html',
  styleUrls: ['./material-escolar.component.css'],
})
export class MaterialEscolarComponent implements OnInit {
  form!: FormGroup;
  checked: boolean = false;
  form1!: FormGroup;
  lista!: string;
  entra: string[] = [];
  lista2!: string;
  borradoAuxiliar: string = '';

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
  }

  ngOnInit(): void {}

  crearFormulario(): void {
    this.form = this.fb.group({
      id: [''],
      utiles: this.fb.array([
        [
          (this.form1 = this.fb.group({
            aceptar: [''],
            nombre: ['', [Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]],
          })),
        ],
      ]),
    });
  }

  get nombreNoValido() {
    return (
      this.form1.get('nombre')?.invalid && this.form1.get('nombre')?.touched
    );
  }

  get utilesEscolares() {
    return this.form.get('utiles') as FormArray;
  }

  agregarUtil(): void {
    this.utilesEscolares.push(this.fb.control(''));
  }

  eliminarUtil(id: number): void {
    this.utilesEscolares.removeAt(id);
  }

  eliminarUtiles(): void {
    this.form = this.fb.group({
      utiles: this.fb.array([]),
    });
  }

  limpiarCajas(): void {
    this.form1.reset({
      nombre: '',
    });
    this.entra = [];
    this.lista2 = this.entra.toString();
  }

  guardarCajas(): void {
    console.log(this.form);
    
    if (this.form1.value.aceptar === true) {
      this.lista = this.form1.value.nombre;
      this.entra.push(this.lista);
      this.lista2 = this.entra.join('\n');
      this.lista2 = this.lista2.split('\n').join('<br>');
      this.form1.patchValue({
        aceptar: '',
      });
    }
  }
}
