import { Component, OnInit } from '@angular/core';
import { listadoTarjetasI } from 'src/app/components/usuario/usuario-interface';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioService } from 'src/app/services/usuario.service';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})

export class UsuarioComponent implements OnInit {

  lisTarjetas: listadoTarjetasI [] = [];
  dataSource!: MatTableDataSource<any>;

  constructor(private _usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.cargarTarjeta();
  }

  cargarTarjeta(){
    this.lisTarjetas = this._UsuarioService.getTarjeta();
    this.dataSource = new MatTableDataSource(this.lisTarjetas)
  }
  eliminarTarjeta(titulo:string){
    console.log(titulo);
    this._usuarioService.eliminarTarjeta(titulo)
    this.cargarTarjeta();
    
  }

}
